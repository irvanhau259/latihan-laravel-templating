<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('halaman.home');
    }
    public function index()
    {
        return view('adminlte.master');
    }
    public function table()
    {
        return view('adminlte.items.table');
    }
    public function datatable()
    {
        return view('adminlte.items.data-table');
    }
}
