<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h2>Buat Account Baru</h2>
    <form action="/kirim" method="POST">
        @csrf
        <label>Firt Name:</label><br>
        <input type="text" name="fnama"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="lnama"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="jnsklmn" value="Laki-Laki">
        <label for="jnsklm">Laki-Laki</label><br>
        <input type="radio" name="jnsklmn" value="Perempuan">
        <label for="jnsklm">Perempuan</label><br><br>
        <label>Nationality</label>
        <select name="negara">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapure">Singapura</option>
            <option value="Malaysia">Malaysia</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="lang" value="Indonesia">
        <label>Indonesia</label><br>
        <input type="checkbox" name="lang" value="English">
        <label>English</label><br>
        <input type="checkbox" name="lang" value="Arabia">
        <label>Arabia</label><br><br>
        <label>Bio</label><br>
        <textarea name="alamat" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Kirim">
    </form>
</body>

</html>